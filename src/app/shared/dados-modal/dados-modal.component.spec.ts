import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DadosModalComponent } from './dados-modal.component';

describe('DadosModalComponent', () => {
  let component: DadosModalComponent;
  let fixture: ComponentFixture<DadosModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DadosModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
