import { Component, OnInit, Input } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-dados-modal',
  templateUrl: './dados-modal.component.html',
  styleUrls: ['./dados-modal.component.scss']
})
export class DadosModalComponent implements OnInit {
 
  @Input() title: string;
  @Input() item: string;
  @Input() closeBtnName: string;
  @Input() imagem: string;
  

  constructor(
    public bsModalRef: BsModalRef
  ) { }

  ngOnInit(): void {
  }
  onClose() {
    this.bsModalRef.hide()
  }
}
