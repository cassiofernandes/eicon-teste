import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  //https://api.themoviedb.org/3/search/movie?api_key=aa7a7d31eb16c5f13021ce3c2df7e19a&language=en-US&query=ice&page=1&include_adult=false
  private key: string = 'aa7a7d31eb16c5f13021ce3c2df7e19a';

  private readonly URL_API = 'https://api.themoviedb.org/3/search/movie?api_key=' + this.key +'&language=';

  private readonly URL_API_DETALHE = 'https://api.themoviedb.org/3/movie/';

  constructor(
    private http: HttpClient
  ) { }

  buscaFilme(linguagem: string, filme: string, page: number) {
    return this.http.get(this.URL_API + linguagem + '&query='+ filme + '&page=' + page);
  }
  detalheDoFilme(id: number, linguagem: string) {
    return this.http.get(this.URL_API_DETALHE + id + '?api_key=' + this.key + '&language=' + linguagem);
  }
}
