import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/api.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DadosModalComponent } from '../shared/dados-modal/dados-modal.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  bsModalRef: BsModalRef;
  subGeral: Subscription;

  visao: boolean;
  //currentPage = 4;
  page: number;
 
  currentPage: number;

  totalRegistro: number;

  guardaIdioma: string;
  imgCerta: string;

  listaFilmes

  imagePath = 'https://image.tmdb.org/t/p/original';

  imgNaoEscontrada: string = 'assets/img/filme.png';

  maxSize: number = 6;

  constructor(
    private service: ApiService,
    private fb: FormBuilder,
    private modalService: BsModalService
  ) { }

  linguagem = [
    { 
      id: 'pt-BR',
      name: 'Português'
    },
    { 
      id: 'en-US',
      name: 'Inglês'
    }
  ]

  ngOnInit(): void {
    this.linguagem;
    this.visao = false;
    this.page = 1;
  }

  formBusca = this.fb.group({
    idioma: ['', Validators.required],
    filme: ['', Validators.required]
  });
  oFilme: string;
  oIdioma: string;

  onSubmit() {
    this.page = 1;
    console.log(this.page, 'w')
    this.listaDeFilme(this.formBusca.value.idioma, this.formBusca.value.filme, this.page);
    
  }

  listaDeFilme(idioma: string, filme: string, page: number) {
    this.subGeral = this.service.buscaFilme(idioma, filme, page)
      .subscribe(res => {
        console.log('ct', res['total_results'])
        this.totalRegistro = res['total_results'];
        this.oIdioma = idioma;
        this.oFilme = filme;
        this.listaFilmes = res['results'];
        this.visao = true;
        this.guardaIdioma = this.formBusca.value.idioma;
        this.formBusca.reset();
      }, err => {
        this.visao = false;
        console.error(err)})   
  }



  detalheFilme(dados){

    let id = dados['id'];
    let info = dados['overview'];
    let idioma = this.guardaIdioma;     
    
    this.service.detalheDoFilme(id, idioma)    
    .subscribe(res => {
        console.log(res)
        this.imgCerta = res['poster_path'];
        this.bsModalRef = this.modalService.show(DadosModalComponent);
        this.bsModalRef.content.title = res['original_title'];
        this.bsModalRef.content.item = info;
        this.bsModalRef.content.imagem =this.imagePath + this.imgCerta; 
        this.bsModalRef.content.closeBtnName = 'Fechar';
        this.bsModalRef.content.control = this.imgCerta;    
    }, err => console.error(err));    
  }
  
  pageChanged(event: any): void {
    console.log(event)
    this.page = event.page;

    this.listaDeFilme(this.formBusca.value.idioma, this.formBusca.value.filme, this.page);
  }

  ngOnDestroy(): void {
    this.subGeral.unsubscribe();
  }

}
